package recur;

public class Rec {
	public static int [] resto(int [] a) {
		int [] aux = new int  [a.length-1];
		for (int i = 1 ; i < a.length ;i++ ) {
			aux[i-1] = a[i];
		}
		return aux;
	}
	public static void imprimir(int [] a) {
		for (int i = 0 ; i < a.length-1 ;i++ ) {
			System.out.print(a[i]+ " ");
		}
		System.out.println(a[a.length-1]);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] a = {1,2,3,4};
		imprimir(resto(a));
	}

}
